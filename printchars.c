#include "csapp.h"
#include "sbuf.h"

int main(int argc, char **argv)
{
	char *filename;
	int fd;
	char c;
	struct stat fileStat;

	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];


	if (stat(filename, &fileStat)<0){	
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else{
		printf("Abriendo archivo %s...\n",filename);
		fd = Open(filename, O_RDONLY, 0);
		while(Read(fd,&c,1)){
			sleep(1);
			printf("Read: %c\n",c);
		}			
		Close(fd);
	}
	
	return 0;
}
